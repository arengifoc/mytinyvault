package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import java.sql.*;
import java.util.Date;
import java.text.SimpleDateFormat;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import classes.*;

public class doLogin extends HttpServlet {
    private static final long serialVersionUID = 3195689946251678970L;
    private static final String DB_DRIVER = ("com.mysql.jdbc.Driver");
    private static final String DB_CONNECTION = ("jdbc:mysql://localhost/db_mytinyvault");
    private static final String DB_USER = ("admin");
    private static final String DB_PASSWORD = ("Peru2020");

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String Email = request.getParameter("Email");
        String Password = request.getParameter("Password");
        UserDAO userdao = new UserDAO();
        PrintWriter stdout = response.getWriter();
        String returnPage;
        Connection connection = null;
        ResultSet result = null;
        PreparedStatement statement = null;
        String sqlquery;

        try {
            User user = null;
            user = userdao.checkLogin(Email, Password);

            // If null, login process has failed
            if (user == null) {
                returnPage = "login.jsp";
                request.setAttribute("message", "Credenciales invalidas");
            }
            // Login has been successful
            else {
                try {
                    Class.forName(DB_DRIVER);
                    connection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);

                    // We need to get the UserID from Email
                    sqlquery = "SELECT UserID FROM User WHERE Email = ?";
                    statement = connection.prepareStatement(sqlquery);
                    statement.setString(1, Email);
                    result = statement.executeQuery();

                    if (result.next()) {
                        String UserID = result.getString("UserID");

                        // Now we need to update LastLogin field for user
                        Date timestamp = new Date();
                        SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String LastLogin = date_format.format(timestamp);
                        sqlquery = "UPDATE User SET LastLogin = ? WHERE UserID = ?";
                        statement = connection.prepareStatement(sqlquery);
                        statement.setString(1, LastLogin);
                        statement.setString(2, UserID);
                        statement.executeUpdate();
                    }

                    result.close();
                    statement.close();
                    connection.close();
                } catch (Exception e) {
                    stdout.print(e);
                }

                returnPage = (user.getIsAdmin() == 1) ? "mantUsers.jsp" : "mantSecrets.jsp";
                HttpSession session = request.getSession();

                // As user.toString() returns the UserID field, we're setting
                // it that as value for "user" attribute of the session which
                // would be unique
                session.setAttribute("user", user.getUserID());
                session.setAttribute("name", user.getName());
                session.setAttribute("isadmin", user.getIsAdmin());
            }

            response.sendRedirect(request.getContextPath() + "/" + returnPage);
        } catch (ClassNotFoundException | SQLException e) {
            throw new ServletException(e);
        }

    }
}
