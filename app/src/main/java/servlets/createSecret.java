package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.protobuf.Value;

import classes.User;

import java.sql.*;

public class createSecret extends HttpServlet {
    /**
     *
     */
    private static final long serialVersionUID = -1241914471555627986L;
    private static final String DB_DRIVER = ("com.mysql.jdbc.Driver");
    private static final String DB_CONNECTION = ("jdbc:mysql://localhost/db_mytinyvault");
    private static final String DB_USER = ("admin");
    private static final String DB_PASSWORD = ("Peru2020");

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);

        if ((session == null) || (session.getAttribute("user") == null) || (session.getAttribute("user") == "")) {
            response.sendRedirect("login.jsp");
        } else {
            Connection connection = null;
            ResultSet result = null;
            PreparedStatement statement = null;
            String sqlquery;
            PrintWriter stdout = response.getWriter();
            int matches = 0;
            String Title = request.getParameter("Title");
            String Notes = request.getParameter("Notes");
            String Value = request.getParameter("Value1");
            String UserID = (String) session.getAttribute("user");
            response.setContentType("text/html");
            stdout.println("<html><body>");
            stdout.println("<br><br>");
            try {
                Class.forName(DB_DRIVER);
                connection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);

                // First, we check if secret with the sam title already exists
                sqlquery = "SELECT COUNT(*) AS Total FROM Secret WHERE Title = ?";
                statement = connection.prepareStatement(sqlquery);
                statement.setString(1, Title);
                result = statement.executeQuery();

                if (result.next()) {
                    matches = Integer.parseInt(result.getString("Total"));

                    // If no matches, then a new secret needs to be created
                    if (matches == 0) {
                        // SQL sentence to create a new secret
                        sqlquery = "INSERT INTO Secret (UserID,Title,Notes) VALUES (?,?,?)";
                        statement = connection.prepareStatement(sqlquery);
                        statement.setString(1, UserID);
                        statement.setString(2, Title);
                        statement.setString(3, Notes);
                        statement.executeUpdate();
                        sqlquery = "SELECT UUID_SHORT()";
                        statement = connection.prepareStatement(sqlquery);
                        result = statement.executeQuery();
                        if (result.next()) {
                            String VersionID = result.getString(1);
                            sqlquery = "SELECT SecretID FROM Secret WHERE Title = ? AND UserID = ?";
                            statement = connection.prepareStatement(sqlquery);
                            statement.setString(1, Title);
                            statement.setString(2, UserID);
                            result = statement.executeQuery();
                            if (result.next()) {
                                String SecretID = result.getString(1);
                                sqlquery = "INSERT INTO Version (VersionID,Value,SecretID) VALUES (?,?,?)";
                                statement = connection.prepareStatement(sqlquery);
                                statement.setString(1, VersionID);
                                statement.setString(2, Value);
                                statement.setString(3, SecretID);
                                statement.executeUpdate();
                                sqlquery = "UPDATE Secret SET CurVerID = ? WHERE SecretID = ?";
                                statement = connection.prepareStatement(sqlquery);
                                statement.setString(1, VersionID);
                                statement.setString(2, SecretID);
                                statement.executeUpdate();
                                // stdout.println("INSERT INTO Secreet (UserID,Title,Notes) VALUES ('" + UserID
                                // + "','" + Title
                                // + "','" + Notes + "')");
                                request.getRequestDispatcher("mantSecrets.jsp").forward(request, response);
                            }
                        }
                    }
                    // Otherwise, secret already exists
                    else {
                        stdout.println("Secreto ya existe<br /><br />");
                        stdout.println("<a href='mantSecrets.jsp'>Volver</a>");
                    }
                }
                result.close();
                statement.close();
                connection.close();
            } catch (Exception e) {
                stdout.print(e + "");
            }

            stdout.println("</body></html>");
            stdout.close();
        }

    }
}
