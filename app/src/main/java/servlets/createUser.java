package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.*;

public class createUser extends HttpServlet {
    /**
     *
     */
    private static final long serialVersionUID = -1241914471555627986L;
    private static final String DB_DRIVER = ("com.mysql.jdbc.Driver");
    private static final String DB_CONNECTION = ("jdbc:mysql://localhost/db_mytinyvault");
    private static final String DB_USER = ("admin");
    private static final String DB_PASSWORD = ("Peru2020");

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Connection connection = null;
        ResultSet result = null;
        PreparedStatement statement = null;
        String sqlquery;
        PrintWriter stdout = response.getWriter();
        String Name = request.getParameter("Name");
        String Email = request.getParameter("Email");
        String Password = request.getParameter("Password1");
        int matches = 0;

        response.setContentType("text/html");
        stdout.println("<html><body>");
        stdout.println("<br><br>");
        try {
            Class.forName(DB_DRIVER);
            connection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);

            // First, we check if user already exists
            sqlquery = "SELECT COUNT(*) AS Total FROM User WHERE Email = ?";
            statement = connection.prepareStatement(sqlquery);
            statement.setString(1, Email);
            result = statement.executeQuery();
            
            if (result.next()) {
                matches = Integer.parseInt(result.getString("Total"));
                
                // If no matches, then a new user needs to be created
                if (matches == 0) {
                    // SQL sentence to create a new user
                    sqlquery = "INSERT INTO User (Email,Name,Password,IsAdmin) VALUES (?,?,?,?)";
                    statement = connection.prepareStatement(sqlquery);
                    statement.setString(1, Email);
                    statement.setString(2, Name);
                    statement.setString(3, Password);
                    statement.setBoolean(4, false);
                    statement.executeUpdate();
                    request.getRequestDispatcher("mantUsers.jsp").forward(request, response);
                }
                // Otherwise, user already exists
                else {
                    stdout.println("Usuario ya existe<br /><br />");
                    stdout.println("<a href='mantUsers.jsp'>Volver</a>");
                }
            }
            result.close();
            statement.close();
            connection.close();
        } catch (Exception e) {
            stdout.print(e + "");
        }

        stdout.println("</body></html>");
        stdout.close();
    }
}
