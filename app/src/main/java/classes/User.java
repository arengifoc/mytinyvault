package classes;

public class User {
    private String UserID;
    private String Email;
    private String Password;
    private String Name;
    private int IsAdmin;

    public User(String UserID, String Name, String Email, int IsAdmin) {
        this.UserID = UserID;
        this.Name = Name;
        this.Email = Email;
        this.IsAdmin = IsAdmin;
    }

    public String toString() {
        return this.UserID;
    }

    public int getIsAdmin() {
        return this.IsAdmin;
    }

    public String getUserID() {
        return this.UserID;
    }

    public void setUserID(String UserID) {
        this.UserID = UserID;
    }

    public String getName() {
        return this.Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getEmail() {
        return this.Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getPassword() {
        return this.Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

}
