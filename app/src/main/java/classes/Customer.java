package classes;

public class Customer extends User {

    public Customer(String UserID, String Name, String Email) {
        super(UserID, Name, Email, 0);
    }

    public void enableUser() {

    }

    public void disableUser() {

    }

    public void deleteUser() {

    }
}
