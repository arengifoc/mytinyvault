package classes;

import java.sql.*;

public class UserDAO {
    private static final String DB_DRIVER = ("com.mysql.jdbc.Driver");
    private static final String DB_CONNECTION = ("jdbc:mysql://localhost/db_mytinyvault");
    private static final String DB_USER = ("admin");
    private static final String DB_PASSWORD = ("Peru2020");

    public User checkLogin(String Email, String Password) throws SQLException, ClassNotFoundException {
        Class.forName(DB_DRIVER);
        Connection connection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);

        User user = null;
        String sqlquery = "SELECT * FROM User WHERE Email = ? and Password = ?";
        PreparedStatement statement = connection.prepareStatement(sqlquery);
        statement.setString(1, Email);
        statement.setString(2, Password);
        ResultSet result = statement.executeQuery();

        if (result.next()) {
            int IsAdmin = Integer.parseInt(result.getString("IsAdmin"));
            if (IsAdmin == 1) {
                user = new Admin(result.getString("UserID"), result.getString("Name"), Email);
            } else {
                user = new Customer(result.getString("UserID"), result.getString("Name"), Email);
            }
        }

        connection.close();
        statement.close();
        result.close();

        return user;
    }
}
