package classes;

public class Admin extends User {

    public Admin(String UserID, String Name, String Email) {
        super(UserID, Name, Email, 1);
    }

    public void enableUser() {

    }

    public void disableUser() {

    }

    public void deleteUser() {

    }
}
