<!DOCTYPE html>
<html lang="en">

<head>
    <%@ include file="WEB-INF/jspf/cmeta.jspf" %>
    <title>MyTinyVault</title>
    <%@ include file="WEB-INF/jspf/cstyles.jspf" %>
</head>

<body>
    <div class="container">
        <%@ include file="WEB-INF/jspf/cnav_nb.jspf" %>

        <h2 align="center">Iniciar sesión</h2>
        <div class="d-flex justify-content-center">
            <form method="POST" action="doLogin" id="loginForm" class="needs-validation" novalidate>
                <div class="form-row">
                    <div class="form-group">
                        <label for="Email">Email</label>
                        <input type="email" class="form-control" id="Email" name="Email" required autofocus>
                        <div class="valid-feedback">
                            Email OK!
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group">
                        <label for="Password">Contraseña</label>
                        <input type="password" class="form-control" name="Password" required>
                        <div class="valid-feedback">
                            Password OK!
                        </div>
                    </div>
                </div>
                <div align="center">
                    <button type="submit" class="btn btn-primary">Ingresar</button>
                </div>
            </form>
        </div>
    </div>
</body>
<%@ include file="WEB-INF/jspf/cjs.jspf" %>

</html>