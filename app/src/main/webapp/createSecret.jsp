<!DOCTYPE html>
<html lang="en">

<head>
    <%@ include file="WEB-INF/jspf/cmeta.jspf" %>
    <title>MyTinyVault</title>
    <%@ include file="WEB-INF/jspf/cstyles.jspf" %>
</head>

<body>
    <div class="container">
        <%@ include file="WEB-INF/jspf/cnav_nb.jspf" %>
        <h2 align="center">Crear secreto</h2>
        <div class="d-flex justify-content-center">
            <form action="createSecret" method="POST" class="needs-validation">
                <div class="form-row col-md-12">
                    <label for="Title">Título</label>
                    <input type="text" class="form-control" id="Title" name="Title" required>
                </div>
                <div class="form-row col-md-12">
                    <label for="Value">Valor</label>
                    <input type="password" class="form-control" id="password1" name="Value1" placeholder="Valor">
                    <i class="fas fa-eye" id="togglePassword1"></i>
                </div>
                <div class="form-row col-md-12">
                    <label for="Value">Confirmar valor</label>
                    <input type="password" class="form-control" id="password2" name="Value2"
                        placeholder="Confirma valor">
                    <i class="fas fa-eye" id="togglePassword2"></i>
                </div>
                <div class="form-row col-md-12">
                    <label for="Notes">Notas</label>
                    <input type="text" class="form-control" id="Notes" name="Notes">
                </div>
                <button type="submit" class="btn btn-primary mb-2" name="saveChanges">Guardar cambios</button>
            </form>
        </div>

        <%@ include file="WEB-INF/jspf/cjs.jspf" %>
    </div>
</body>

</html>