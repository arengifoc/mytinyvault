<%@page import="java.sql.*"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <%@ include file="WEB-INF/jspf/cmeta.jspf" %>
    <title>MyTinyVault</title>
    <%@ include file="WEB-INF/jspf/cstyles.jspf" %>
</head>

<body>
    <%
    if ((session == null) || (session.getAttribute("user") == null) || (session.getAttribute("user") == "")) {
        response.sendRedirect("login.jsp");
    }
    else {
        final String DB_DRIVER = ("com.mysql.jdbc.Driver");
        final String DB_CONNECTION = ("jdbc:mysql://localhost/db_mytinyvault");
        final String DB_USER = ("admin");
        final String DB_PASSWORD = ("Peru2020");
        String SecretID = request.getParameter("SecretID");

        Connection connection = null;
        PreparedStatement statement = null;
        
        try {
            Class.forName(DB_DRIVER);
            connection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
            String sqlquery = "DELETE FROM Secret WHERE SecretID = ?";
            statement = connection.prepareStatement(sqlquery);
            statement.setString(1, SecretID);
            statement.executeUpdate();
            connection.close();
            statement.close();
            request.getRequestDispatcher("mantSecrets.jsp").forward(request, response);
        } catch (Exception e) {
            out.print(e+"");
        }
    }
    %>
</body>

</html>