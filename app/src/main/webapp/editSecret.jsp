<%@page import="java.sql.*"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>

<!DOCTYPE html>
<html lang="en">

<head>
    <%@ include file="WEB-INF/jspf/cmeta.jspf" %>
    <title>MyTinyVault</title>
    <%@ include file="WEB-INF/jspf/cstyles.jspf" %>
</head>

<body>
    <div class="container">
        <%@ include file="WEB-INF/jspf/cnav_adm.jspf" %>
        <%
            if ((session == null) || (session.getAttribute("user") == null) || (session.getAttribute("user") == "")) {
                response.sendRedirect("login.jsp");
            }
            else {
        %>

        <h2 align="center">Editar secreto</h2>
        <%
                String SecretID=request.getParameter("SecretID");
                Connection connection=null;
                PreparedStatement statement=null;
                ResultSet result=null;
                String sqlquery;
                
                try {
                    final String DB_DRIVER = ("com.mysql.jdbc.Driver");
                    final String DB_CONNECTION = ("jdbc:mysql://localhost/db_mytinyvault");
                    final String DB_USER = ("admin");
                    final String DB_PASSWORD = ("Peru2020");
                    
                    Class.forName(DB_DRIVER);
                    connection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
                    sqlquery = "SELECT L.UserID,R.SecretID,L.Title,L.Notes,R.Value,R.VersionID FROM Secret L JOIN Version R ON L.CurVerID = R.VersionID WHERE L.SecretID = ?";
                    statement = connection.prepareStatement(sqlquery);
                    statement.setString(1, SecretID);
                    result = statement.executeQuery();
                    
                    if (result.next()) {
        %>
        <div class="d-flex justify-content-center">
            <form action="" class="needs-validation">
                <div class="form-row col-md-12">
                    <label for="Title">Título</label>
                    <input type="text" class="form-control" id="Title" name="Title"
                        value='<%=result.getString("Title")%>' required>
                </div>
                <div class="form-row col-md-12">
                    <label for="Value">Valor</label>
                    <input type="password" class="form-control" id="password1" name="Value1"
                        value='<%=result.getString("Value")%>'>
                    <i class="fas fa-eye" id="togglePassword1"></i>
                </div>
                <div class="form-row col-md-12">
                    <label for="Value">Confirmar valor</label>
                    <input type="password" class="form-control" id="password2" name="Value2"
                        value='<%=result.getString("Value")%>'>
                    <i class="fas fa-eye" id="togglePassword2"></i>
                </div>
                <div class="form-row col-md-12">
                    <label for="Notes">Notas</label>
                    <input type="text" class="form-control" id="Notes" name="Notes"
                        value='<%=result.getString("Notes")%>'>
                </div>
                <input type="hidden" id="SecretID" name="SecretID" value="<%=SecretID%>">
                <input type="hidden" id="VersionID" name="VersionID" value='<%=result.getString("VersionID")%>''>
                <button type="submit" class="btn btn-primary mb-2" name="saveChanges">Guardar cambios</button>
            </form>
        </div>
        <%
                    }
                    if (request.getParameter("saveChanges") != null) {
                        SecretID = request.getParameter("SecretID");
                        String VersionID = request.getParameter("VersionID");
                        String Value = request.getParameter("Value1");
                        String Title = request.getParameter("Title");
                        String Notes = request.getParameter("Notes");
    
                        try {
                            // Make changes to Secret attributes (Title and Notes) but not its Version content yet
                            sqlquery = "UPDATE Secret SET Title = ?, Notes = ? WHERE SecretID = ?";
                            statement = connection.prepareStatement(sqlquery);
                            statement.setString(1, Title);
                            statement.setString(2, Notes);
                            statement.setString(3, SecretID);
                            statement.executeUpdate();

                            try {
                                // Make changes to Secret attributes (Title and Notes) but not its Version content yet
                                Date timestamp = new Date();
                                SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                String LastUpdate = date_format.format(timestamp);
                                sqlquery = "UPDATE Version SET Value = ?, LastUpdate = ? WHERE VersionID = ?";
                                statement = connection.prepareStatement(sqlquery);
                                statement.setString(1, Value);
                                statement.setString(2, LastUpdate);
                                statement.setString(3, VersionID);
                                statement.executeUpdate();
                                //out.print("UPDATE Version SET Value = '" + Value + "', LastUpdate = '" + LastUpdate + "' WHERE VersionID = '" + VersionID + "'");
                                response.sendRedirect(request.getContextPath() + "/mantSecrets.jsp");
                            } catch (Exception e) {
                                out.print(e);
                            }
                        } catch (Exception e) {
                            out.print(e);
                        }
                    }
                    connection.close();
                    statement.close();
                    result.close();
                } catch (Exception e) {
                    out.print(e);
                }
            }
        %>
    </div>
</body>
<%@ include file="WEB-INF/jspf/cjs.jspf" %>
</html>