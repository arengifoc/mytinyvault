<!DOCTYPE html>
<html lang="en">

<head>
    <%@ include file="WEB-INF/jspf/cmeta.jspf" %>
    <title>MyTinyVault</title>
    <%@ include file="WEB-INF/jspf/cstyles.jspf" %>
</head>

<body>
    <div class="container">
        <%@ include file="WEB-INF/jspf/cnav_nb.jspf" %>
        <section>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-3 col-xs-0"></div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <form method="POST" action="createUser" class="needs-validation">
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="Name">Nombres</label>
                                <input type="text" class="form-control" id="Name" name="Name" autofocus required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="Email">Email</label>
                                <input type="email" class="form-control" id="Email" name="Email" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="Password1">Contraseña</label>
                                <input type="password" class="form-control" name="Password1" id="password1"
                                    required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="Password2">Repetir contraseña</label>
                                <input type="password" class="form-control" name="Password2" id="password2"
                                    required>
                            </div>
                        </div>
                        <div align="center">
                            <button type="submit" class="btn btn-primary">Enviar</button>
                        </div>
                    </form>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-3 col-xs-0"></div>
        </section>
        <%@ include file="WEB-INF/jspf/cjs.jspf" %>
    </div>
</body>

</html>