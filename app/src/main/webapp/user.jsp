<!DOCTYPE html>
<html lang="en">

<head>
    <%@ include file="WEB-INF/jspf/cmeta.jspf" %>
    <title>MyTinyVault</title>
    <%@ include file="WEB-INF/jspf/cstyles.jspf" %>
</head>

<body>
    <div class="container">
        <%@ include file="WEB-INF/jspf/cnav_usr.jspf" %>
        <section>

        </section>
        <!-- <form action="createUser" method="POST">

        </form> -->
        <%@ include file="WEB-INF/jspf/cjs.jspf" %>
    </div>
</body>

</html>