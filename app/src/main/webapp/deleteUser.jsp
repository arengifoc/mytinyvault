<%@page import="java.sql.*"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <%@ include file="WEB-INF/jspf/cmeta.jspf" %>
    <title>MyTinyVault</title>
    <%@ include file="WEB-INF/jspf/cstyles.jspf" %>
</head>

<body>
    <%
    if ((session == null) || (session.getAttribute("user") == null) || (session.getAttribute("user") == "")) {
        response.sendRedirect("login.jsp");
    }
    else if (((Integer) session.getAttribute("isadmin")).equals(0)) {
        response.sendRedirect("mantSecrets.jsp");
    }
    else {
        final String DB_DRIVER = ("com.mysql.jdbc.Driver");
        final String DB_CONNECTION = ("jdbc:mysql://localhost/db_mytinyvault");
        final String DB_USER = ("admin");
        final String DB_PASSWORD = ("Peru2020");
        String CurrentUserID = (String) session.getAttribute("user");
    
        String UserID=request.getParameter("UserID");

        if (!UserID.equals(CurrentUserID)) {
            Connection connection=null;
            ResultSet result=null;
            PreparedStatement statement=null;
            
            try {
                Class.forName(DB_DRIVER);
                connection=DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
                String sqlquery="DELETE FROM User WHERE UserID = ?";
                statement = connection.prepareStatement(sqlquery);
                statement.setString(1, UserID);
                statement.executeUpdate();
                connection.close();
                result.close();
                statement.close();
            } catch (Exception e) {
                out.print(e+"");
            }
        }
        request.getRequestDispatcher("mantUsers.jsp").forward(request, response);
    }
    %>
</body>

</html>