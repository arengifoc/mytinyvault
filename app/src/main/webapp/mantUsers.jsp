<%@page import="java.sql.*"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <%@ include file="WEB-INF/jspf/cmeta.jspf" %>
    <title>MyTinyVault</title>
    <%@ include file="WEB-INF/jspf/cstyles.jspf" %>
</head>

<body>
    <div class="container">
        <%@ include file="WEB-INF/jspf/cnav_adm.jspf" %>
        <%
            if ((session == null) || (session.getAttribute("user") == null) || (session.getAttribute("user") == "")) {
                response.sendRedirect("login.jsp");
            }
            else if (((Integer) session.getAttribute("isadmin")).equals(0)) {
                response.sendRedirect("mantSecrets.jsp");
            }
            else {
        %>
        <hr>
        <table class="table table-bordered table-hover">
            <thead class="thead-dark" align="center">
                <tr>
                    <th colspan="4" class="align-middle">Mantenimiento de usuarios</th>
                    <th><a href="createUser.jsp"><img height="30" src="resources/images/create-user.png"
                                title="Crear usuario" alt="Crear usuario"></a></th>
                </tr>
                <tr>
                    <th scope="col">UserID</th>
                    <th scope="col">Email</th>
                    <th scope="col">Name</th>
                    <th scope="col">Último login</th>
                    <th scope="col">Acción</th>
                </tr>
            </thead>
            <tbody>
                <%
                    Connection connection=null;
                    Statement statement=null;
                    ResultSet result=null;
                    try {
                        final String DB_DRIVER = ("com.mysql.jdbc.Driver");
                        final String DB_CONNECTION = ("jdbc:mysql://localhost/db_mytinyvault");
                        final String DB_USER = ("admin");
                        final String DB_PASSWORD = ("Peru2020");
                        
                        Class.forName(DB_DRIVER);
                        connection=DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
                        statement=connection.createStatement();
                        result=statement.executeQuery("SELECT UserID,Email,Name,LastLogin FROM User");
                        
                        while (result.next()) {
                %>
                <tr>
                    <td align="center"><%=result.getString(1)%></td>
                    <td align="center"><%=result.getString(2)%></td>
                    <td align="left"><%=result.getString(3)%></td>
                    <td align="center"><%=result.getString(4)%></td>
                    <td align="center"><a href="editUser.jsp?UserID=<%=result.getString(1)%>">Editar</a> | <a
                            href="deleteUser.jsp?UserID=<%=result.getString(1)%>">Eliminar</a></td>
                </tr>
                <%
                        }
                        statement.close();
                        result.close();
                        connection.close();
                    } catch (Exception e) {
                        out.print(e);
                    }
                }
                %>

            </tbody>
        </table>
        <%@ include file="WEB-INF/jspf/cjs.jspf" %>
    </div>
</body>

</html>