<!DOCTYPE html>
<html lang="en">

<head>
    <%@ include file="WEB-INF/jspf/cmeta.jspf" %>
    <title>MyTinyVault</title>
    <%@ include file="WEB-INF/jspf/cstyles.jspf" %>
</head>

<body>
    <div class="container">
        <%@ include file="WEB-INF/jspf/cnav_nb.jspf" %>
        <h2 align="center">Crear usuario</h2>
        <div class="d-flex justify-content-center">
            <form method="POST" action="createUser" class="needs-validation">
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="Name">Nombres</label>
                        <input type="text" class="form-control" id="Name" name="Name" autofocus required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="Email">Email</label>
                        <input type="email" class="form-control" id="Email" name="Email" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="Password1">Contraseña</label>
                        <input type="password" class="form-control" name="Password1" id="mkuser_pass_1" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="Password2">Repetir contraseña</label>
                        <input type="password" class="form-control" name="Password2" id="mkuser_pass_2" required>
                    </div>
                </div>
                <div align="center">
                    <button type="submit" class="btn btn-primary">Crear</button>
                    <a href="mantUsers.jsp" class="btn btn-secondary" role="button">Cancelar</a>
                </div>
            </form>
        </div>

        <%@ include file="WEB-INF/jspf/cjs.jspf" %>
    </div>
</body>

</html>