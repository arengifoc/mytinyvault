(function () {
  'use strict';
  window.addEventListener('load', function () {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function (form) {
      form.addEventListener('submit', function (event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();

var formpass1 = document.getElementById("password1"),
  formpass2 = document.getElementById("password2");

function validatePassword() {
  if (formpass1.value != formpass2.value) {
    formpass2.setCustomValidity("Passwords no coinciden");
  } else {
    formpass2.setCustomValidity('');
  }
}

formpass1.onchange = validatePassword;
formpass2.onkeyup = validatePassword;

const togglePassword1 = document.querySelector('#togglePassword1');
const pass1 = document.querySelector('#password1');
const togglePassword2 = document.querySelector('#togglePassword2');
const pass2 = document.querySelector('#password2');

togglePassword1.addEventListener('click', function (e) {
  // toggle the type attribute
  const type1 = pass1.getAttribute('type') === 'password' ? 'text' : 'password';
  pass1.setAttribute('type', type1);
  // toggle the eye slash icon
  this.classList.toggle('fa-eye-slash');
});

togglePassword2.addEventListener('click', function (e) {
  // toggle the type attribute
  const type2 = pass2.getAttribute('type') === 'password' ? 'text' : 'password';
  pass2.setAttribute('type', type2);
  // toggle the eye slash icon
  this.classList.toggle('fa-eye-slash');
});