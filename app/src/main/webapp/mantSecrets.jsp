<%@page import="java.sql.*"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <%@ include file="WEB-INF/jspf/cmeta.jspf" %>
    <title>MyTinyVault</title>
    <%@ include file="WEB-INF/jspf/cstyles.jspf" %>
</head>

<body>
    <div class="container">
        <%@ include file="WEB-INF/jspf/cnav_usr.jspf" %>
        <%
            if ((session == null) || (session.getAttribute("user") == null) || (session.getAttribute("user") == "")) {
                response.sendRedirect("login.jsp");
            }
            else {
        %>
        <hr>
        <table class="table table-bordered table-hover">
            <thead class="thead-light" align="center">
                <tr>
                    <th colspan="3" class="align-middle">Mantenimiento de secretos</th>
                    <th><a href="createSecret.jsp"><img height="30" src="resources/images/create-secret.png"
                                title="Crear secreto" alt="Crear secreto"></a></th>
                </tr>
                <tr>
                    <th scope="col">Título</th>
                    <th scope="col">Notas</th>
                    <th scope="col">Última actualización</th>
                    <th scope="col">Acción</th>
                </tr>
            </thead>
            <tbody>
                <%
                Connection connection = null;
                PreparedStatement statement = null;
                ResultSet result=null;
                try {
                    final String DB_DRIVER = ("com.mysql.jdbc.Driver");
                    final String DB_CONNECTION = ("jdbc:mysql://localhost/db_mytinyvault");
                    final String DB_USER = ("admin");
                    final String DB_PASSWORD = ("Peru2020");
                    
                    Class.forName(DB_DRIVER);
                    connection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
                    String UserID = (String) session.getAttribute("user");
                    String sqlquery = "select R.SecretID,L.Title,L.Notes,R.LastUpdate from Secret L JOIN Version R ON L.CurVerID = R.VersionID WHERE L.UserID = ?";
                    statement = connection.prepareStatement(sqlquery);
                    statement.setString(1, UserID);
                    result = statement.executeQuery();
                    
                    while (result.next()) {
                        %>
                <tr>
                    <td align="center"><%=result.getString("Title")%></td>
                    <td align="center"><%=result.getString("Notes")%></td>
                    <td align="left"><%=result.getString("LastUpdate")%></td>
                    <td align="center"><a href="editSecret.jsp?SecretID=<%=result.getString(1)%>">Editar</a> | <a
                            href="deleteSecret.jsp?SecretID=<%=result.getString(1)%>">Eliminar</a></td>
                </tr>
                <%
                        }
                        statement.close();
                        result.close();
                        connection.close();
                    } catch (Exception e) {
                        out.print(e);
                    }
                                    
            }
                    %>

            </tbody>
        </table>
        <%@ include file="WEB-INF/jspf/cjs.jspf" %>
    </div>
</body>

</html>