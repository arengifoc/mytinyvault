<%@page import="java.sql.*"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <%@ include file="WEB-INF/jspf/cmeta.jspf" %>
    <title>MyTinyVault</title>
    <%@ include file="WEB-INF/jspf/cstyles.jspf" %>
</head>

<body>
    <div class="container">
        <%@ include file="WEB-INF/jspf/cnav_adm.jspf" %>
        <%
            if ((session == null) || (session.getAttribute("user") == null) || (session.getAttribute("user") == "")) {
                response.sendRedirect("login.jsp");
            }
            else {
        %>
        <h2 align="center">Editar usuario</h2>
        <%
                String UserID=request.getParameter("UserID");
                Connection connection=null;
                Statement statement=null;
                ResultSet result=null;
                try {
                    final String DB_DRIVER = ("com.mysql.jdbc.Driver");
                    final String DB_CONNECTION = ("jdbc:mysql://localhost/db_mytinyvault");
                    final String DB_USER = ("admin");
                    final String DB_PASSWORD = ("Peru2020");

                    Class.forName("com.mysql.jdbc.Driver");
                    connection=DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
                    statement=connection.createStatement();
                    result=statement.executeQuery("SELECT * FROM User WHERE UserID='"+UserID+"'");

                    while (result.next()) {
        %>
        <form action="" class="needs-validation">
            <div class="form-group">
                <label for="UserID">UserID</label>
                <input type="text" class="form-control" id="UserID" name="UserID" value="<%=result.getString(1)%>"
                    readonly>
            </div>
            <div class="form-group">
                <label for="Email">Email</label>
                <input type="text" class="form-control" id="Email" name="Email" value="<%=result.getString(2)%>" required>
            </div>
            <div class="form-group">
                <label for="Name">Nombres</label>
                <input type="text" class="form-control" id="Name" name="Name" value="<%=result.getString(3)%>" required>
            </div>
            <div class="form-group">
                <label for="Password1">Contraseña</label>
                <input type="password" class="form-control" id="password1" name="Password1"
                    value="<%=result.getString(4)%>" required>
            </div>
            <div class="form-group">
                <label for="Password2">Confirmar contraseña</label>
                <input type="password" class="form-control" id="password2" value="<%=result.getString(4)%>" required>
            </div>
            <button type="submit" class="btn btn-primary mb-2" name="saveChanges">Guardar cambios</button>
        </form>
        <%
                    }
                } catch (Exception e) {
                    out.print(e);
                }
                if (request.getParameter("saveChanges") != null) {
                    UserID=request.getParameter("UserID");
                    String Email=request.getParameter("Email");
                    String Name=request.getParameter("Name");
                    String Password=request.getParameter("Password1");

                    statement.executeUpdate("UPDATE User SET Email='"+Email+"',Name='"+Name+"',Password='"+Password+"' WHERE UserID='"+UserID+"'");
                    request.getRequestDispatcher("mantUsers.jsp").forward(request, response);
                    connection.close();
                    statement.close();
                    result.close();
                }
            }
        %>
        <%@ include file="WEB-INF/jspf/cjs.jspf" %>
    </div>
</body>

</html>