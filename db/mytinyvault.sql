-- Temporary, it must be removed later
DROP DATABASE IF EXISTS db_mytinyvault;

CREATE DATABASE IF NOT EXISTS db_mytinyvault;

USE db_mytinyvault;

CREATE TABLE IF NOT EXISTS `User` (
  `UserID` bigint unsigned default(uuid_short()) NOT NULL,
  `Email` varchar(40) NOT NULL,
  `Name` varchar(40) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `FbURL` varchar(100),
  `AvURL` varchar(100),
  `IsAdmin` boolean NOT NULL,
  `LastLogin` timestamp default CURRENT_TIMESTAMP,
  PRIMARY KEY (UserID)
) ENGINE=InnoDB;

INSERT INTO `User` (Email,Name,Password,IsAdmin) VALUES ('admin@utp.edu.pe','Usuario Admin','12345678', true);
INSERT INTO `User` (Email,Name,Password,IsAdmin) VALUES ('user1@utp.edu.pe','Usuario uno','12345678', false);
INSERT INTO `User` (Email,Name,Password,IsAdmin) VALUES ('user2@utp.edu.pe','Usuario dos','12345678', false);
INSERT INTO `User` (Email,Name,Password,IsAdmin) VALUES ('user3@utp.edu.pe','Usuario tres','12345678', false);
INSERT INTO `User` (Email,Name,Password,IsAdmin) VALUES ('user4@utp.edu.pe','Usuario cuatro','12345678', false);
INSERT INTO `User` (Email,Name,Password,IsAdmin) VALUES ('user5@utp.edu.pe','Usuario cinco','12345678', false);

CREATE TABLE IF NOT EXISTS `Secret` (
  `SecretID` bigint unsigned default(uuid_short()) NOT NULL,
  `Title` varchar(40) NOT NULL,
  `CurVerID` bigint unsigned,
  `UserID` bigint UNSIGNED NOT NULL,
  `Notes` varchar(255),
  `CryptKey` varchar(40),
  PRIMARY KEY (SecretID),
  FOREIGN KEY (UserID)
  REFERENCES User(UserID)
  ON DELETE CASCADE
  ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `Version` (
  `VersionID` bigint unsigned default(uuid_short()) NOT NULL,
  `Value` varchar(40) NOT NULL,
  `LastUpdate` timestamp default CURRENT_TIMESTAMP,
  `SecretID` bigint unsigned NOT NULL,
  PRIMARY KEY (VersionID),
  FOREIGN KEY (SecretID)
  REFERENCES Secret(SecretID)
  ON DELETE CASCADE
  ON UPDATE CASCADE
) ENGINE=InnoDB;

-- SET @user_id = (SELECT UserID FROM User WHERE Email='arengifoc@utp.edu.pe');
-- INSERT INTO `Secret` (UserID,Title,Notes) VALUES (@user_id,'Facebook','Mi cuenta firme');
-- INSERT INTO `Secret` (UserID,Title,Notes) VALUES (@user_id,'Gmail','');
-- INSERT INTO `Secret` (UserID,Title,Notes) VALUES (@user_id,'Paypal','asociada con Tarjeta Visa CMR');

-- SET @secret_id = (SELECT SecretID FROM Secret WHERE Title='Facebook' AND UserID=@user_id);

-- SET @version_id = uuid_short();
-- INSERT INTO `Version` (VersionID,Value,SecretID) VALUES (@version_id, 'misecreto1', @secret_id);
-- UPDATE Secret SET CurVerID = @version_id WHERE SecretID = @secret_id;
