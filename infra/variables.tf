variable "tags" {
  default = {
    comment = "Demo for Java app"
    owner = "angel.rengifo"
  }
}
variable "zone_names" { default = ["us-east-1b","us-east-1c"] }
variable "network_name" { default = "vault-network" }
variable "network_cidr" { default = "10.0.0.0/16" }
variable "public_subnet_cidrs" { default = ["10.0.1.0/24"] }
variable "private_subnet_cidrs" { default = ["10.0.2.0/24","10.0.3.0/24"] }
variable "region" { default = "us-east-1" }
variable "public_sshkey" {}
variable "appserver-vm-name" { default = "appserver" }
variable "appserver-vm-size" { default = "t3a.small" }
variable "appserver-vm-userdata" { default = null }
variable "internal_lb" { default = true }

variable "git_repo" { default = "https://gitlab.com/arengifoc/mytinyvault.git" }


variable "db_identifier" { default = "appserver" }
variable "db_engine" { default = "mariadb" }
variable "db_engine_version" { default = "10.4.13" }
variable "db_engine_major_version" { default = "10.4" }
variable "db_engine_family" { default = "mariadb10.4" }
variable "db_instance_size" { default = "db.t3.small" }
variable "db_disk_size" { default = 5 }
variable "db_name" {}
variable "db_port" { default = "3306" }
variable "db_master_username" {}
variable "db_master_password" {}
variable "db_maintenance_window" { default = "Mon:00:00-Mon:03:00" }
variable "db_backup_window" { default = "03:00-06:00" }
variable "db_backup_retention" { default = 0 }
