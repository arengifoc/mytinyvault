terraform {
  backend "s3" {
    bucket  = "arengifoc-tfstate"
    key     = "dev/mytinyvault/terraform.tfstate"
    region  = "us-east-1"
    profile = "arengifoc"
  }
}

provider "aws" {
  region = var.region
}

