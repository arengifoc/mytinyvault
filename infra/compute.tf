resource "aws_key_pair" "keypair" {
  key_name_prefix = "vault"
  public_key      = var.public_sshkey
}

module "ami" {
  source = "git::https://gitlab.com/arengifoc/terraform-aws-data-amis"
  os     = "Ubuntu Server 18.04"
}

module "sg_appserver" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "3.16.0"

  name         = "sg_appserver"
  description  = "SG for appserver hosts"
  vpc_id       = module.network.vpc_id
  egress_rules = ["all-all"]
  ingress_with_cidr_blocks = [
    {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      description = "SSH service"
      cidr_blocks = "0.0.0.0/0"
    },
    {
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      description = "HTTP service"
      cidr_blocks = "0.0.0.0/0"
    },
    {
      from_port   = 443
      to_port     = 443
      protocol    = "tcp"
      description = "HTTPS service"
      cidr_blocks = "0.0.0.0/0"
    }
  ]
}

module "ec2_appserver" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "2.15.0"

  instance_count              = 1
  name                        = var.appserver-vm-name
  ami                         = module.ami.id
  instance_type               = var.appserver-vm-size
  key_name                    = aws_key_pair.keypair.key_name
  monitoring                  = false
  vpc_security_group_ids      = [module.sg_appserver.this_security_group_id]
  subnet_ids                  = module.network.public_subnets
  associate_public_ip_address = true
  tags                        = var.tags
  user_data = templatefile(
    "${path.module}/appserver_setup.sh.tpl",
    {
      git_repo           = var.git_repo,
      db_name            = var.db_name,
      db_master_username = var.db_master_username,
      db_master_password = var.db_master_password
    }
  )
}
