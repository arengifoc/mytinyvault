#!/bin/bash
apt-get update
apt-get install -y git maven nginx tomcat9 tomcat9-admin tomcat9-examples software-properties-common
apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8
add-apt-repository "deb [arch=amd64,arm64,ppc64el] http://mariadb.mirror.liquidtelecom.com/repo/10.4/ubuntu $(lsb_release -cs) main"
apt-get install -y mariadb-server
apt-get clean
mkdir /root/.m2
cat > /root/.m2/settings.xml <<EOF
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd">
  <servers>
    <server>
      <id>TomcatServer</id>
      <username>admin</username>
      <password>admin</password>
    </server>
  </servers>
</settings>
EOF
sed -i -e '/^<\/tomcat-users/d' /etc/tomcat9/tomcat-users.xml

cat >> /etc/tomcat9/tomcat-users.xml <<EOF
<role rolename="manager-gui"/>
<role rolename="manager-script"/>
<role rolename="manager-jmx"/>
<user username="admin" password="admin" roles="manager-gui,manager-script,manager-jmx"/>
</tomcat-users>
EOF
systemctl restart tomcat9
cat > /etc/nginx/sites-enabled/default <<EOF
server {
	server_name _;
	listen 80 default_server;
	listen [::]:80 default_server;

  location / {
    return 301 \$scheme://\$host/app;
  }

  location /app/ {
        proxy_set_header X-Forwarded-Host \$host;
        proxy_set_header X-Forwarded-Server \$host;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_pass       http://127.0.0.1:8080/app/;
  }
}
EOF
systemctl restart nginx
mkdir /tmp/mytinyvault
cd /tmp/mytinyvault
git clone -b devel ${git_repo} .
sed -i -e '/\[mysqld\]/adefault-time-zone = "-05:00"' /etc/mysql/my.cnf
systemctl restart mariadb
mysql < /tmp/mytinyvault/db/mytinyvault.sql
mysql -e "CREATE USER ${db_master_username}@localhost identified by '${db_master_password}';"
mysql -e "GRANT ALL ON ${db_name}.* TO ${db_master_username}@localhost ; FLUSH PRIVILEGES;"
cd /tmp/mytinyvault/app
mvn tomcat7:deploy